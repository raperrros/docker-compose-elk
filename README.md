# ElasticSearch, Logstash, Kibana (ELK) #

With this docker-compose repository you can install dockerized ELK stack just in seconds.
ES, Logstash and Kibana are locked to version 7.11.1

# Data Retention #

You can configure data retention in kibana by specifying the desired parameters in the kibana/config/kibana.yml configuration.

https://www.elastic.co/guide/en/kibana/7.11/settings.html

# Requirements #

Docker engine, git, docker-compose, enough RAM on your host to handle the load (minimal RAM required: 2GB)
Kibana web interface will be available on the 5601/tcp port when sucessfully started.

# Installation #

#### Make sure your setup meets minimal requirements ####

See section Requirements above

#### Clone this repository ####

`git clone git@gitlab.com:raperrros/docker-compose-elk.git`

#### Change logstash.yml permissions ####

`chmod 777 logstash/config/logstash.yml`

Otherwise you will experience permissions issue

#### Setup password ####

- Create ENV file as follows

- Make sure you change your_password with a password you want to use

- DO NOT change username

elk.env:
```
#elasticsearch
ELASTIC_USERNAME="elastic"
ELASTIC_PASSWORD="your_password"

#kibana
ELASTICSEARCH_USERNAME="elastic"
ELASTICSEARCH_PASSWORD="your_password"

#logstash
XPACK_MONITORING_ELASTICSEARCH_USERNAME="elastic"
XPACK_MONITORING_ELASTICSEARCH_PASSWORD="your_password"

```
- Also set your password in file logstash/pipeline/logstash.conf

#### Logstash pipeline config ####

Logstash is needed to collect, filter and normalize logs. The Logstash configuration file format contains 3 parts.

1. input {} is the input point for logs. It determines through which channels the logs will get into Logstash. There are only three types: file, udp, tcp.

2. filter {} This block configures basic log manipulations. This can be a breakdown by key = value, and removing unnecessary parameters, and replacing existing values, and using geoip or DNS queries for ip addresses or host names.

3. output {} specifies the settings for outgoing messages. Similar to the previous blocks, any number of outbound sub-blocks can be specified here.

4. The output also specifies a password from elastic so that logstash can access it. Provided you have xpack enabled.

#### Start your ELK stack ####

Run `docker-compose up -d` to start your ELK stack

#### Ready to use ####

Access Kibana by visiting your host's 5601/tcp via HTTP

#### Start pushing logs ####

You can now push your application logs to your ELK stack to UDP port 5000 or others you might configure in logstash/pipeline/logstash.conf

# Securing access #

For production use you definitely need to have HTTPS on for accessing Kibana
This section roughly describes how you can secure it with nginx
You'd need ether an nginx proxy from another server or docker to forward and encypt requests to your ELK stack

Configure virtualhost config like this but replace SERVERNAME and DOCKER_HOST_IP:

```
upstream kibana {
    server DOCKER_HOST_IP:5601;
  }

  server {
    listen 80;
    server_name SERVERNAME;
    return 301 https://$server_name:443$request_uri;
    client_max_body_size 8M;
    client_body_buffer_size 64K;
    fastcgi_buffers 64 4K;
  }

  server {
    listen 443 ssl;
    server_name SERVERNAME;
    ssl_certificate  /etc/letsencrypt/live/SERVERNAME/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/SERVERNAME/privkey.pem;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
    ssl_ecdh_curve secp384r1;
    ssl_session_cache shared:SSL:10m;
    ssl_session_tickets off;
    ssl_stapling on;
    ssl_stapling_verify on;
    resolver 8.8.8.8 8.8.4.4 valid=300s;
    resolver_timeout 5s;

##################################### WARNING! WARNING! WARNING! #################################
############################## READ ABOUT THE FOLLOWING SETTING ABOUT ############################
############## HOW IT AFFECTS YOUR WEBSITE VISITORS BEFORE RUNNING PRODUCTION MODE! ##############
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security#preloading_strict_transport_security
    #add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";
    add_header Strict-Transport-Security "max-age=63072000; includeSubdomains";

    add_header X-Frame-Options DENY;
    add_header X-Content-Type-Options nosniff;
    ssl_dhparam /etc/nginx/ssl/dhparam.pem;
    client_max_body_size 8M;
    client_body_buffer_size 64K;
    fastcgi_buffers 64 4K;

    location / {
      proxy_pass http://kibana;
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $scheme;
    }
  }
```

